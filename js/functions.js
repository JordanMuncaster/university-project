var myPie;
var kCalTotal;
var fatTotal = 0;
var carbTotal = 0;
var proteinTotal = 0;
var energyTotal = 0;

function addToTable() {
	retrieveFood($("#searchBox").val());
	return false;
}

function retrieveFood(foodName) {
	$.getJSON( 'php/retrieveFood.php', {
		foodName	:	foodName
	}, function(data){
		
	}).done(function(data) {
		if (data.Name != null) {
			$("#foodTable").append("<tr> <td>" + 
					data['Name'] + "</td> <td>" + 
					data['Fat'] + "</td> <td>" +
				 	data['Carbohydrates'] + "</td> <td>" + 
				 	data['Proteins'] + "</td> <td>" +
					data['Energy'] + "</td> </tr>"); 
			loadChart(data['Fat'],data['Carbohydrates'],data['Proteins'], data['Energy']);
			
		}
	});
}

function loadChart(fats, carbs, proteins, energy) {

	var food = new foodPercents(fats, carbs, proteins, energy);

	var pieData = 
	 	[{
	        value : food.fats,
	        color : "#374140",
	        label : 'Fats %',
	        labelColor : 'white',
	        labelFontSize : '12'
	    },
	  	{
	        value : food.carbs,
	        color : "#D9CB9E",
	        label : 'Carbs %',
	        labelColor : 'white',
	        labelFontSize : '12'
	    },
	    {
	        value : food.proteins,
	        color : "#DC3522",
	        label : 'Proteins %',
	        labelColor : 'white',
	        labelFontSize : '12'
	    },
	];

	Chart.defaults.global.segmentShowStroke = false;
	Chart.defaults.global.segmentStrokeWidth = 0;

	if ($("#myChart").is(":visible")) {
		myPie.segments[0].value = food.fats;
		myPie.segments[1].value = food.carbs;
		myPie.segments[2].value = food.proteins;
		// Would update the first dataset's value of 'Green' to be 10
		myPie.update();
		$("#dietName").html(calculateDiet(food));

	} else {
		// alert("new");
		$("#myChart").fadeIn(1000);
		$("#tableAndChart").css('padding-top', '100px');
		$("#foodTable").fadeIn(1000);
		$("#dietNameHolder").fadeIn(1000);
		$("#message").hide();
		
		$(".intro-header").animate({"max-height":"150px", "position":"fixed", "width":"100%"}, 500);
		$(".site-heading").animate({"padding-top":"100px"}, 500);
		myPie = new Chart(document.getElementById("myChart").getContext("2d")).Pie(pieData, { 		               
				animationSteps: 100,
				animationEasing: 'easeInOutQuart'	
		});
		$("#dietName").html(calculateDiet(food));
	}
}

function foodPercents (fats, carbs, proteins, energy) {
	fatTotal += parseInt(fats);
	carbTotal  += parseInt(carbs);
	proteinTotal  += parseInt(proteins);

	this.energy = Math.round((fatTotal * 9) + (proteinTotal * 4) + (carbTotal * 4));
	this.fats = Math.round(((fatTotal * 9) / this.energy) * 100);
	this.proteins = Math.round(((proteinTotal * 4) / this.energy) * 100);
	this.carbs = Math.round(((carbTotal * 4) / this.energy) * 100);

	// alert(this.energy + " | " + this.fats + " | " + this.proteins + " | " + this.carbs);

}

// Calculates the diet based upon what macros where input
function calculateDiet(food) {

	if (food.fats == 50 && food.carbs == 50 && food.proteins == 0) {
		return "Atkins";
	} else if (food.fats >= 10 && food.fats <= 35 && food.carbs >= 20 && food.carbs <= 25 && food.proteins >= 45 && food.proteins <=65) {
		return "Weight Watchers";
	} else if (food.fats == 30 && food.carbs == 40 && food.proteins == 30) {
		return "Zone Diet";
	} else if (food.fats == 25 && food.carbs == 50 && food.proteins == 25) {
		return "Text Book";
	} else if (food.fats == 73 && food.carbs == 0 && food.proteins == 27) {
		return "My First Diet";
	} else {
		return "We're sorry but we couldn't find a diet based upon the input food.";
	}
}