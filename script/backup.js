var request	 	= require('request'),
    cheerio 	= require('cheerio'),
    nodemailer 	= require('nodemailer'),
    mysql		= require('mysql'),
    fs 			= require('fs');

// Script settings
var sleepTime 		= 	1000; // How long the script takes between each interaction with a webpage
var toNotify 		= 	"jordanmuncaster@gmail.com"; // Email to notfiy on success/failure
var numItems 		= 	0; // Number of items scanned and imported into the database
var outOf 			=	0; // Number of items scanned in total
var catsToIgnore 	= 	[		
							"Baby", 
							"Health & beauty", 
							"Pet care", 
							"Household", 
							"Home & entertainment"
						]; // Categories for the script to ignore

// Database connection details
var connection = mysql.createConnection({
	host     : 'localhost',
	database : 'project',
	user     : 'root',
	password : '',
});

// Emailer system
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user	: 'jordan.muncaster.project@gmail.com',
        pass 	: 'project11'
    }
});

function food(energy, fat, saturates, sugars, salts, proteins) {
	this.energy = energy;
	this.fat = fat;
	this.saturates = saturates;
	this.sugars = sugars;
	this.salts = salts;
	this.proteins = proteins;
}

food.prototype.isEmpty = function() {
	if (this.energy == null && this.fat == null && this.saturates == null
	&& this.sugars == null && this.salts == null && this.proteins == null) {
		return true;
	}
}

// Looks into Tesco's groceries section and scans relevent pages
request('http://www.tesco.com/groceries/', function (error, response, body) {
	if (!error && response.statusCode == 200) {
		var $ = cheerio.load(body);

		$('span.hide').each(function () {
			var a = $(this).parent();
			var text = a.text();
			var skip = false;

			for (var i = 0; i < catsToIgnore.length; i++) {
				if (text.indexOf(catsToIgnore[i]) > -1) {
					skip = true;
					console.log("Ignoring > " + text);
				}
			}

			var url = a.attr('href');
			if (skip!= true && url != null && url.toString().indexOf('/groceries/') > -1 ) {
				console.log(url);

				request(url, function (error, response, body) {

					var $ = cheerio.load(body);

					console.log("Getting: " + text);

					$('.column').each(function () {
						$(this).each(function() {
							var columns =  $(this).children().children().children();
							if (columns != "") {
								columns.each(function() {
									console.log("	Getting: " + $(this).html());
									var url = $(this).attr('href');
									scanPage(url);

								});
							}
						})
					});
				});
			}
		});
	}
});

process.on('uncaughtException', function(err) {
	console.log('Caught exception: ' + err);
});

function scanPage(url) {
	
	request(url, function (error, response, body) {
		var $ = cheerio.load(body);
		$('#multipleAdd .allProducts ').children('.productLists').children().children().each(function () {
			url = "http://www.tesco.com/" + $(this).children().children().children().attr('href');

			request(url, function (error, response, body) {
				var $ = cheerio.load(body);
				// sleep(sleepTime);
				var name = ($('.desc h1').text());
				console.log("Working with: " + url);
				// console.log("URL: " + url);
				if ($('.productDetails').children('.detailsWrapper').last().children('h2').text() =="Nutrition") 
				{
				var foodInput = new food();

				$('.productDetails').last('.detailsWrapper').children('.detailsWrapper')
				.last().children().children().children('tbody').children('tr')
				.each(function() {
					if ($(this).children().first().text() != '') {		
						switch ($(this).children().first().text().toString().split("(")[0].trim()) {
							case "":
							case "- kcal":
							case "kcal":
							case "-":
								if ($(this).children('td').first().text().toString().indexOf('kcal') > -1) {
									foodInput.energy = $(this).children('td').first().text().replace("(", " ");							
								}
								break;
							case "Fat":
								foodInput.fat = $(this).children('td').first().text().toString().replace(/[^0-9.]/g, "");
								break;
							case "of which saturates":
							case "of which are Saturates":
							case "Saturates":
								foodInput.saturates = $(this).children('td').first().text().toString().replace(/[^0-9.]/g, "");
								break;
							case "Sugars":
								foodInput.sugars = $(this).children('td').first().text().toString().replace(/[^0-9.]/g, "");
								break;
							case "Salt":
								foodInput.salts = $(this).children('td').first().text().toString().replace(/[^0-9.]/g, "");
								break;
							case "Protein":
								foodInput.proteins = $(this).children('td').first().text().toString().replace(/[^0-9.]/g, "");
								break;
						}
					}
				});


				if (!foodInput.isEmpty()) {
					var post  = {
						Name 		: 	name, 
						Nickname	: 	'', 
						Description : 	'', 
						Energy 		: 	foodInput.energy, 
						Fat 		: 	foodInput.fat, 
						Saturates 	: 	foodInput.saturates, 
						Proteins	: 	foodInput.proteins, 
						Salts		: 	foodInput.salts
					};
					var query = connection.query('INSERT INTO foods SET ?', post, function(err, result) {
						console.log("Added To Database");
						numItems++;
					});	
				}


				} else if ($('.guidelineDailyAmount').text() != '') {
					nutritionalValue = ($('span .gdaValue'));

					var foodInput = new food();

					nutritionalValue.each(function () {
						switch ($(this).parent().parent().children('h3').text()) {
							case "Energy":
								foodInput.energy = $(this).text().toString().split("kJ")[1].split("kcal")[0];
								break;
							case "Fat":
								foodInput.fat = $(this).text().toString().replace(/[^0-9.]/g, "");
								break;
							case "Saturates":
								foodInput.saturates = $(this).text().toString().replace(/[^0-9.]/g, "");
								break;
							case "Sugars":
								foodInput.sugars = $(this).text().toString().replace(/[^0-9.]/g, "");
								break;
							case "Salt":
								foodInput.salts = $(this).text().toString().replace(/[^0-9.]/g, "");
								break;
							case "Protein":
								foodInput.proteins = $(this).text().toString().replace(/[^0-9.]/g, "");
								break;
						}
					});
					
					if (!foodInput.isEmpty()) {
						var post  = {
							Name 		: 	name, 
							Nickname	: 	'', 
							Description : 	'', 
							Energy 		: 	foodInput.energy, 
							Fat 		: 	foodInput.fat, 
							Saturates 	: 	foodInput.saturates, 
							Proteins	: 	foodInput.proteins, 
							Salts		: 	foodInput.salts
						};

						var query = connection.query('INSERT INTO foods SET ?', post, function(err, result) {
							// Added into the database.
							console.log("Added To Database");
							numItems++;
						});		
					}
				} else {
					// console.log(name + " | " + "Found something else ... ");
				}
			});
		});
	});
}


// Sleeps the thread for x milliseconds, removes chance of domain timeout.
function sleep(milliseconds) {
	var start = new Date().getTime();
	for (var i = 0; i < 1e7; i++) {
		if ((new Date().getTime() - start) > milliseconds) {
			break;
		}
	}
}




// var mailOptions = {
// 		    from: 'Your Final Year Project <jordan.muncaster.project@gmail.com>', // sender address
// 		    to: toNotify, // list of receivers
// 		    subject: "There was an error with your Script.", // Subject line
// 		    text: 'Error.', // plaintext body
// 		    html: '<b>More Details To Come.</b><br><br>' + url // html body
// 		};
// transporter.sendMail(mailOptions, function(error, info){
//     if(error){
//         console.log(error);
//     }else{
    	
// 	}
// });

// console.log(e);
// process.exit(1);
