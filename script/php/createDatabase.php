<?php 
    $dbhost  = '127.0.0.1';
    $dbname  = 'project';
    $dbuser  = 'root';
    $dbpass  = '';

    mysql_connect($dbhost, $dbuser, $dbpass) or die(mysql_error());
    $db_selected = mysql_select_db($dbname);

    if (!$db_selected) {
        $sql = 'CREATE DATABASE ' . $dbname;

        if (mysql_query($sql)) {
            echo "Database " . $dbname . " created successfully\n";
            // Once the database has successfully been created, it selects it.
            $db_selected = mysql_select_db($dbname);
        } else {
            echo 'Error creating database: ' . mysql_error() . "\n";
        }
    }
    
    // Create the food table.
    createTable('foods', 
                'id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                Name varchar(255) DEFAULT NULL,
                Nickname varchar(255) DEFAULT NULL,
                Description varchar(255) DEFAULT NULL,
                Energy decimal(6,2) DEFAULT NULL,
                Fat decimal(6,2) DEFAULT NULL,
                Saturates decimal(6,2) DEFAULT NULL,
                Proteins decimal(6,2) DEFAULT NULL,
                Salts decimal(6,2) DEFAULT NULL');


    function createTable($name, $query) {
        queryMysql("CREATE TABLE IF NOT EXISTS $name($query)");
    }

    function queryMysql($query) {
        $result = mysql_query($query) or die(mysql_error());
    	 return $result;
    }

?>