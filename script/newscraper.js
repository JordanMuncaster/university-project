var request = require('request'),
    cheerio = require('cheerio'),
    nodemailer = require('nodemailer'),
    mysql = require('mysql'),
    fs = require('fs');
// Script settings
var sleepTime = 1000; // How long the script takes between each interaction with a webpage
var toNotify = "jordanmuncaster@gmail.com"; // Email to notfiy on success/failure
var NumItemsInput = 0; // Number of items scanned and imported into the database
var numItemsScanned = 0; // Number of items scanned 
var numPagesScanned = 0; // Number of pages scanned
var errorsFound = 0; // Number of errors found whilst scanning
var timeElapsed = new Date().getTime(); // Used to determine how long the script has been running
var catsToIgnore = ["Baby", "Health & beauty", "Pet care", "Household", "Home & entertainment"]; // Categories for the script to ignore
// Currently the project runs continiously and doesn't ever stop, Fall back plan: Have it so if 
//	it hasn't scanned a product for 5 seconds or so it closes and ends the script.
// Database connection details
var connection = mysql.createConnection({
    host: 'localhost',
    database: 'project',
    user: 'root',
    password: '',
});
// Emailer system
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'jordan.muncaster.project@gmail.com',
        pass: 'project11'
    }
});

function food(energy, fat, saturates, sugars, salts, proteins, carbohydrates) {
    this.energy = energy;
    this.fat = fat;
    this.saturates = saturates;
    this.sugars = sugars;
    this.salts = salts;
    this.proteins = proteins;
    this.carbohydrates = carbohydrates;
}
food.prototype.isEmpty = function() {
    if (this.energy == null && this.fat == null && this.saturates == null && this.sugars == null && this.salts == null && this.proteins == null) {
        return true;
    }
}
// Looks into Tesco's groceries section and scans relevent pages
request('http://www.tesco.com/groceries/', function(error, response, body) {
    if (!error && response.statusCode == 200) {
        var $ = cheerio.load(body);
        $('span.hide').each(function() {
            var a = $(this).parent();
            var text = a.text();
            var skip = false;
            for (var i = 0; i < catsToIgnore.length; i++) {
                if (text.indexOf(catsToIgnore[i]) > -1) {
                    skip = true;
                    console.log("Ignoring > " + text);
                }
            }
            var url = a.attr('href');
            if (skip != true && url != null && url.toString().indexOf('/groceries/') > -1) {
                // Pulls out each URL that has the URL /gorceries/
                request(url, function(error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var $ = cheerio.load(body);
                        console.log("Getting: " + text);
                        $('.column').each(function() {
                            $(this).each(function() {
                                var columns = $(this).children().children().children();
                               if (columns != "") {
                                    columns.each(function() {
                                        var url = $(this).attr('href');
                                        console.log(url);
                                        scanPage(url);
                                    });
                                }
                            })
                        });
                    }
                });
            }
        });
    }
});
process.on('uncaughtException', function(err) {
    console.log('Caught exception: ' + err);
    errorsFound++;
});

function scanPage(url) {
    request(url, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            var $ = cheerio.load(body);
            $('#multipleAdd .allProducts ').children('.productLists').children().children().each(function() {
                url = "http://www.tesco.com/" + $(this).children().children().children().attr('href');
                request(url, function(error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var $ = cheerio.load(body);
                        numItemsScanned++;
                        // sleep(sleepTime);
                        var name = ($('.desc h1').text());
                        var foodInput = new food();
                        //!!!IMPORTANT		Add Carbs
                        if ($('.productDetails').children('.detailsWrapper').last().children('h2').text() == "Nutrition") {
                            $('.productDetails').last('.detailsWrapper').children('.detailsWrapper').last().children().children().children('tbody').children('tr').each(function() {
                                if ($(this).children().first().text() != '') {
                                    switch ($(this).children().first().text().toString().split("(")[0].trim().toUpperCase()) {
                                        case "":
                                        case "- KCAL":
                                        case "KCAL":
                                        case "-":
                                            foodInput.energy = $(this).children('td').first().text().replace("(", " ");
                                            foodInput.energy = foodInput.energy.split("/")[0];
                                            break;
                                        case "FAT":
                                        case "TOTAL FAT":
                                        case "TOTAL FAT OF WHICH":
                                            foodInput.fat = $(this).children('td').first().text().toString().replace(/[^0-9.]/g, "");
                                            break;
                                            //Some work is needed here to neaten up (Ignorecase/TOUPPERCASE)
                                        case "OF WHICH SATURATES":
                                        case "OF WHICH ARE SATURATES":
                                        case "SATURATED FAT":
                                        case "of WHICH SATURATES (G)":
                                        case "(OF WHICH SATURATES)":
                                        case "SATURATES":
                                        case "- OF WHICH SATURATES":
                                        case "OF WHICH: SATURATES":
                                        case "OF WHICH SATURATES":
                                            foodInput.saturates = $(this).children('td').first().text().toString().replace(/[^0-9.]/g, "");
                                            break;
                                        case "SUGARS":
                                            foodInput.sugars = $(this).children('td').first().text().toString().replace(/[^0-9.]/g, "");
                                            break;
                                        case "SALT":
                                        case "AS SALT":
                                        case "EQUIVALENT AS SALT":
                                        case "*SALT EQUIVALENT":
                                        case "(EQUIVALENT AS SALT)":
                                            foodInput.salts = $(this).children('td').first().text().toString().replace(/[^0-9.]/g, "");
                                            break;
                                        case "CARBOHYDRATES":
                                        case "CARBOHYDRATE":
                                        case "TOTAL CARBOHYDRATE":
                                            foodInput.carbohydrates = $(this).children('td').first().text().toString().replace(/[^0-9.]/g, "");
                                            break;
                                        case "PROTEINS":
                                        case "PROTEIN":
                                            foodInput.proteins = $(this).children('td').first().text().toString().replace(/[^0-9.]/g, "");
                                            break;
                                    }
                                }
                            });
                        } else if ($('.guidelineDailyAmount').text() != '') {
                            nutritionalValue = ($('span .gdaValue'));
                            nutritionalValue.each(function() {
                                switch ($(this).parent().parent().children('h3').text()) {
                                    case "Energy":
                                        foodInput.energy = $(this).text().toString().split("kJ")[1].split("kcal")[0];
                                        break;
                                    case "Fat":
                                        foodInput.fat = $(this).text().toString().replace(/[^0-9.]/g, "");
                                        break;
                                    case "Saturates":
                                        foodInput.saturates = $(this).text().toString().replace(/[^0-9.]/g, "");
                                        break;
                                    case "Sugars":
                                        foodInput.sugars = $(this).text().toString().replace(/[^0-9.]/g, "");
                                        break;
                                    case "Salt":
                                        foodInput.salts = $(this).text().toString().replace(/[^0-9.]/g, "");
                                        break;
                                    case "Protein":
                                        foodInput.proteins = $(this).text().toString().replace(/[^0-9.]/g, "");
                                        break;
                                }
                            });
                        }
                        if (!foodInput.isEmpty()) {
                            var post = {
                                Name: name,
                                Nickname: '',
                                Description: '',
                                Energy: foodInput.energy,
                                Fat: foodInput.fat,
                                Saturates: foodInput.saturates,
                                Proteins: foodInput.proteins,
                                Salts: foodInput.salts,
                                Carbohydrates: foodInput.carbohydrates
                            };
                            var query = connection.query('INSERT INTO foods SET ?', post, function(err, result) {
                                NumItemsInput++;
                            });
                        }
                    }
                });
            });
        }
        // Checks if there is a next page, if yes it reccursivly searches until there is no next page
        var nextPage = $('#multipleAdd').children('.top').children('.controlsBar').children('.pagination').children('.clearfix').children('.nextWrap').children().children().attr('href');
        if (nextPage) {
            numPagesScanned++;
            scanPage(nextPage);
            console.log("NEXT PAGE FOUND: " + numPagesScanned);
        }
    });
}

function msToTime(duration) {
    var milliseconds = parseInt((duration % 1000) / 100),
        seconds = parseInt((duration / 1000) % 60),
        minutes = parseInt((duration / (1000 * 60)) % 60),
        hours = parseInt((duration / (1000 * 60 * 60)) % 24);
    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;
    return hours + ":" + minutes + ":" + seconds + "." + milliseconds;
}
// Script Finish Email
function notifySuccess() {
    var mailOptions = {
        from: 'Your Final Year Project <jordan.muncaster.project@gmail.com>', // sender address
        to: toNotify, // list of receivers
        subject: "Script Finished Running", // Subject line
        text: "", // plaintext body
        html: 'Hi Jordan, your script has finished running. <br>I\'ve managed to import <b>' + NumItemsInput + '</b> out of a total of <b>' + numItemsScanned + '</b> items scanned. There was <b>' + errorsFound + '</b> Errors Found.<br>Time elapsed: <b>' + msToTime(new Date().getTime() - timeElapsed) + '</b>.' // html body
    };
    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log(error);
        } else {
            process.exit(1);
        }
    });
}

function notifyFailure() {
    var mailOptions = {
        from: 'Your Final Year Project <jordan.muncaster.project@gmail.com>', // sender address
        to: toNotify, // list of receivers
        subject: "There was an error with your Script.", // Subject line
        text: 'Error.', // plaintext body
        html: '<b>More Details To Come.</b><br><br>' + url // html body
    };
    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log(error);
        } else {
            process.exit(1);
        }
    });
}